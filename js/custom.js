$(function(){
	$('.tabs a').click(function(){
		$(this).parents('.tab-wrap').find('.tab-cont').addClass('hide');
		$(this).parent().siblings().removeClass('active');
		var id = $(this).attr('href');
		$(id).removeClass('hide');
		$(this).parent().addClass('active');
		return false;
	});
	$(".js-menu-btn").click(function(){
		$(".js-menu-btn").toggleClass('active');
		$(".navigation").slideToggle();
		return false;
	});
});

$(function() {
	$('.js-portfolio-gallery').magnificPopup({
		delegate: 'a',
		type: 'image',
		tLoading: 'Loading image #%curr%...',
		mainClass: 'mfp-img-mobile',
		gallery: {
			arrowMarkup: '<button title="%title%" type="button" class="mfp-arrow mfp-arrow-%dir%"></button>',
			enabled: true,
			navigateByImgClick: false,
			preload: [0,1] // Will preload 0 - before current, and 1 after the current image
		},
		image: {
			tError: '<a href="%url%">Изображение #%curr%</a> не удалось загрузить.',
			titleSrc: function(item) {
				return item.el.attr('title');
			}
		},

		callbacks: {
			close: function() {
			// Will fire when popup is closed
				alert('Спасибо!');
			}
			// e.t.c.
		}
	});
});

$(function () {
	$('.popup-modal').magnificPopup({
		type: 'inline',
		preloader: false,
		focus: '#username',
		modal: true
	});
	$(document).on('click', '.popup-modal-dismiss', function (e) {
		e.preventDefault();
		$.magnificPopup.close();
	});
});

$(function () {
	$('.slider').slick({
	  dots: true,
	  infinite: true,
	  speed: 300,
	  slidesToShow: 1,
	  adaptiveHeight: false
	});
});

$(function () {
	$('.js-form').submit(function( event ) {
		if (($('#name').val() ==="")||($('#tel').val() === "")) {
			if ($('#name').val() === "") {
				$('#name').addClass('error');		
			} 
			if ($('#tel').val() === "") {
				$('#tel').addClass('error');
			}	
			return false;
		}
		else {
			$('#name').val('');
			$('#tel').val('');
			$.magnificPopup.open({
				items: {
					src: '#test-modal5'
				},
				type: 'inline'
			}, 0);
			$('#name').removeClass('error');
			$('#tel').removeClass('error');
		}
		event.preventDefault();	
	});
});

